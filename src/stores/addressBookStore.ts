import { ref} from 'vue'
import { defineStore } from 'pinia'
interface AddressBook {
  id: number
  name: string
  tel: string
  gender: string
}

export const useAddressBookStore = defineStore('address_book', () => {
  let lastId = 1
  const address = ref<AddressBook>({
    id: 0,
    name: '',
    tel: '',
    gender: 'Male'
  })
  const addressList = ref<AddressBook[]>([])
  const isAddNew = ref(false)
  function save() {
    if (address.value.id > 0) {
      //Edit
      const editedIndex = addressList.value.findIndex((item) => item.id === address.value.id)
      addressList.value[editedIndex] = address.value
    } else {
      // Add new
      addressList.value.push({ ...address.value, id: lastId++ })
    }
  
    isAddNew.value = !isAddNew.value
    address.value = {
      id: 0,
      name: '',
      tel: '',
      gender: 'Male'
    }
  }
  
  function edit(id: number) {
    isAddNew.value = true
    const editedIndex = addressList.value.findIndex((item) => item.id === id)
    address.value = JSON.parse(JSON.stringify(addressList.value[editedIndex])) // Copy Object JSON.parse(JSON.stringify(object))
  }
  
  function remove(id: number) {
    const deletedIndex = addressList.value.findIndex((item) => item.id === id)
    addressList.value.splice(deletedIndex, 1)
  }
  function cancel() {
    address.value = {
      id: 0,
      name: '',
      tel: '',
      gender: 'Male'
    }
    isAddNew.value = !isAddNew.value
  }

  return { address, addressList, save, isAddNew, edit, remove, cancel }
})
